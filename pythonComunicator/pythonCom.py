import sys
import struct
import os, msvcrt
import datetime
import json

def log_interno(message):
    with open('c:\\temp\\log.txt', mode='a') as file:
        file.write("{data} - {msg}\r\n".format(data=str(datetime.datetime.now()), msg=message))
        file.flush()

class PythonCom(object):
    def __init__(self):
        self.receivers = []

    def register_receiver(self, receiver):
        self.receivers.append(receiver)

    def notify_receivers(self, cmd, data):
        for r in self.receivers:
            r(self, cmd, data)

    def send_message(self, success, data, f=sys.stdout.buffer):
        s = json.dumps({ 'data': data, 'success': success })
        s = s.encode('utf-8')
        f.write(struct.pack('<I', len(s)))
        f.write(s)
        f.flush()

    def recv_message(self, f=sys.stdin.buffer):
        buf = f.read(4)
        xlen, = struct.unpack('<I', buf)
        return f.read(xlen)

    def recv_loop(self):
        while True:
            msg = self.recv_message()
            log_interno(msg)
            j = json.loads(msg.decode('utf-8'))
            self.notify_receivers(j['cmd'], j['data'])

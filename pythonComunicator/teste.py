import sys
import struct
import os, msvcrt
import datetime
import json

import pythonCom

class Teste(object):

    def __init__(self, comunicator):
        self.aux = 0
        self.comunicator = comunicator

    def receive(self, sender, cmd, data):

        if cmd == 'run':
            self.comunicator.send_message(True, { 'result': cmd + ' recebido.'})
        else:
            if cmd == 'sum':
                self.aux += data
                self.comunicator.send_message(True, { 'result': 'Soma: ' + str(self.aux) + 'recebido.'})
            else:
                self.comunicator.send_message(False, 'Erro desconhecido.')


if __name__ == "__main__":
    
    comun = pythonCom.PythonCom();

    teste = Teste(comun)
    comun.register_receiver(teste.receive)
    comun.recv_loop()

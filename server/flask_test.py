#levanta servidor

from flask import Flask, send_file
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello, world!"
    

@app.route("/bye")
def bye():
    return "Good bye, cruel world!"

@app.route("/teste")
def teste():
    return send_file('index.html')

if __name__ == "__main__":
    app.run(use_reloader=True, debug=True, use_debugger=True)


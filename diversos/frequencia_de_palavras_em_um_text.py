# contador e frequencia de cada palavra em um texto

import nltk
from nltk.book import *
from operator import itemgetter

# nota: para usar um text qualquer, tenho que tokeniza-lo. Não posso usar o texto diretamente pois ele vai trabalhar com letras e não com palavras
text3 = "O telefone caiu e despedaçou completamente, deixando de ser um telefone."
text3 = nltk.word_tokenize(text3)

# o map não executa as funções especificadas
#(item, frequencia, percentual de vezes que aparece)
counter = map(lambda x: (x, text3.count(x), float(text3.count(x))/len(text3)), set(text3))

print("Calculating...")
# apenas quando se cria uma lista, ou itera sobre o iterable é que os cálculos são feitos
calculated = list(counter)
print("Calculating done.")

##for item in counter:
##    print(item)

#max count
max_count = max(calculated, key=itemgetter(1))
print(max_count)

#max frequency
max_frequency = max(calculated, key=itemgetter(2))
print(max_frequency)

#Existe uma função pronta para frequencia
# nltk.FreqDist(text3)


# coding: utf-8

# In[88]:

import sympy
sympy.init_printing()
x = sympy.var('x')

sympy.sin(x**2).integrate(x)


# In[17]:

import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0, 100)
line, = plt.plot(x, np.sin(x), linewidth=2)

dashes = [10, 5, 100, 5] # 10 points on, 5 off, 100 on, 5 off
line.set_dashes(dashes)

plt.show()

# In[112]:

x = "I code in Matlab".replace('Matlab','Python')
x += ("."*30)
print(x)
print(x[::-1])

a = ', '
b = [('2', '2', '2'), ('2', '2', '2')]
c = a.join(''.join(i) for i in b)
print(c)

print([(i, j, k) for i in range(10) for j in range(10) if i + j == 10 for k in range(5)])

x = []
for i in range(10):
    for j in range(10):
        if i + j == 10:
            for k in range(5):
                x.append((i, j, k))
            
print(x)


# In[102]:

for i in range(10):
    print(i)


# In[119]:

a, *b = 1, 2, 3

x = [(a, b)] * 2

print(a, b)
print(x)


# In[ ]:




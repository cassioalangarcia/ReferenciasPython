import urllib.request, urllib.parse, urllib.error, ast

##"codigoTabelaReferencia=176&codigoMarca=23&codigoModelo=4380&codigoTipoVeiculo=1&anoModelo=2009&codigoTipoCombustivel=1&tipoVeiculo=carro&modeloCodigoExterno=&tipoConsulta=tradicional"
##params = {
##    "codigoTabelaReferencia": 176,
##    "codigoMarca": 23,
##    "codigoModelo": 4380,
##    "codigoTipoVeiculo": 1,
##    "anoModelo": 2009,
##    "codigoTipoCombustivel": 1,
##    "tipoVeiculo": "carro",
##    "modeloCodigoExterno": "",
##    "tipoConsulta": "tradicional"
##    }
##
##query = urllib.parse.urlencode(params)
##query = query.encode('UTF-8')

query = "codigoTabelaReferencia=176&codigoMarca=23&codigoModelo=4380&codigoTipoVeiculo=1&anoModelo=2009&codigoTipoCombustivel=1&tipoVeiculo=carro&modeloCodigoExterno=&tipoConsulta=tradicional"
query = query.encode('UTF-8')

url = "http://www2.fipe.org.br/IndicesConsulta-ConsultarValorComTodosParametros"

#POST
req = urllib.request.Request(url, query)

#GET: passa os parâmetros junto da url (www.site.com/servico?a=1&b=2)

responseData = urllib.request.urlopen(req).read().decode('utf8', 'ignore')

responseData = ast.literal_eval(responseData)

for key in responseData:
    print("%s: %s" % (key.ljust(20), responseData[key]))

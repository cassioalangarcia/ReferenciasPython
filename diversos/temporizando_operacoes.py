import timeit

init = 'import functools; aux = range(1, 5001)'

print(timeit.timeit('s = functools.reduce(lambda x, y: x + y, aux)', init, number = 1000))

print(timeit.timeit('f = functools.reduce(lambda x, y: x * y, aux)', init, number = 1000))

# uso da lib nltk para as tarefas de pré-processamento de texto

import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

def _calculate_languages_ratios(text):
    """
    Calculate probability of given text to be written in several languages and
    return a dictionary that looks like {'french': 2, 'spanish': 4, 'english': 0}
    
    @param text: Text whose language want to be detected
    @type text: str
    
    @return: Dictionary with languages and unique stopwords seen in analyzed text
    @rtype: dict
    """

    languages_ratios = {}

    '''
    nltk.word_tokenize() splits all punctuations into separate tokens
    '''

    # == tokenização ==
    tokens = word_tokenize(text)

    # == normalização para lower case ==
    words = [word.lower() for word in tokens]

    # Compute per language included in nltk number of unique stopwords appearing in analyzed text
    for language in stopwords.fileids():
        stopwords_set = set(stopwords.words(language))
        words_set = set(words)
        common_elements = words_set.intersection(stopwords_set)

        languages_ratios[language] = len(common_elements) # language "score"

    return languages_ratios

# == detecção de idioma ==
def detect_language(text):
    """
    Calculate probability of given text to be written in several languages and
    return the highest scored.
    
    It uses a stopwords based approach, counting how many unique stopwords
    are seen in analyzed text.

    @param text: Text whose language want to be detected
    @type text: str
    
    @return: Most scored language guessed
    @rtype: str
    """

    ratios = _calculate_languages_ratios(text)

    most_rated_language = max(ratios, key=ratios.get)

    return most_rated_language

def stop_words_removal(text):

    # == tokenização ==
    tokens = word_tokenize(text)

    en_stop_words = set(stopwords.words('english'))

    text_without_stop_words = [w for w in tokens if w not in en_stop_words]

    return(" ".join(text_without_stop_words))

def stemm(text):

    # == tokenização ==
    tokens = word_tokenize(text)

    porter_stemmer = nltk.stem.porter.PorterStemmer()

    return [porter_stemmer.stem_word(x) for x in tokens]


if __name__=='__main__':

    # testar diferentes textos apenas para a tarefa de detecção de idioma
    # para as demais, cada idioma é trabalhado diferente
    text_en = """
    Information retrieval
    Information retrieval (IR) is the activity of obtaining information resources relevant to an information need from a collection of information resources. Searches can be based on metadata or on full-text (or other content-based) indexing.
    Automated information retrieval systems are used to reduce what has been called "information overload". Many universities and public libraries use IR systems to provide access to books, journals and other documents. Web search engines are the most visible IR applications.
    Overview
    An information retrieval process begins when a user enters a query into the system. Queries are formal statements of information needs, for example search strings in web search engines. In information retrieval a query does not uniquely identify a single object in the collection. Instead, several objects may match the query, perhaps with different degrees of relevancy.
    An object is an entity that is represented by information in a database. User queries are matched against the database information. Depending on the application the data objects may be, for example, text documents, images, audio, mind maps or videos. Often the documents themselves are not kept or stored directly in the IR system, but are instead represented in the system by document surrogates or metadata.
    Most IR systems compute a numeric score on how well each object in the database matches the query, and rank the objects according to this value. The top ranking objects are then shown to the user. The process may then be iterated if the user wishes to refine the query.
    """

    text_pt = """
    Recuperação de informação (RI) é uma área da computação que lida com o armazenamento de documentos e a recuperação automática de informação associada a eles. É uma ciência de pesquisa sobre busca por informações em documentos, busca pelos documentos propriamente ditos, busca por metadados que descrevam documentos e busca em banco de dados, sejam eles relacionais e isolados ou banco de dados interligados em rede de hipermídia, tais como a World Wide Web. A mídia pode estar disponível sob forma de textos, de sons, de imagens ou de dados. Há, entretanto, muita confusão entre os termos e conceitos "recuperação de dados", "recuperação de documentos", "recuperação de informações" e "recuperação de textos". Na verdade, cada um destes é uma área especial que possui seu próprio corpo de conhecimento e literatura, teoria, praxis e tecnologias.
    """

    text_ge = """
    Information Retrieval [ˌɪnfɚˈmeɪʃən ɹɪˈtɹiːvəl] (IR) bzw. Informationsrückgewinnung, gelegentlich ungenau Informationsbeschaffung, ist ein Fachgebiet, das sich mit computergestütztem Suchen nach komplexen Inhalten (also z. B. keine Einzelwörter) beschäftigt und in die Bereiche Informationswissenschaft, Informatik und Computerlinguistik fällt. Wie aus der Wortbedeutung von retrieval (deutsch Abruf, Wiederherstellung) hervorgeht, sind komplexe Texte oder Bilddaten, die in großen Datenbanken gespeichert werden, für Außenstehende zunächst nicht zugänglich oder abrufbar. Beim Information Retrieval geht es darum, bestehende Informationen aufzufinden, nicht neue Strukturen zu entdecken (wie beim Knowledge Discovery in Databases, zu dem das Data-Mining und Text Mining gehören).
    """

    print(" - Texto utilizado: {t}".format(t=text_en))
    
    language = detect_language(text_en)
    print(" - Idioma detectado: {i}\n".format(i=language))

    stop_word_less = stop_words_removal(text_en)
    print(" - Texto sem stop words: {swl}".format(swl=stop_word_less))

    stammed_words = stemm(stop_word_less);
    print(" - Stemming: {st}".format(st=stammed_words))

    

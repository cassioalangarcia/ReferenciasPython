
from tkinter import *

global clicks3
clicks3 = 0

def update_count():
    global clicks3
    clicks3 += 1
    print("Num clicks =", clicks3)

#create the window
root = Tk()

#modify root window
root.title("Labeler")
root.geometry("800x600")

app = Frame(root)
app.grid()

label = Label(app, text = "This is a lable")
label.grid()

button1 = Button(app, text = "this is a button")
button1.grid()

button2 = Button(app)
button2.grid()
button2.configure(text = "this is another button")

button3 = Button(app)
button3.grid()
button3["text"] = "this is one more button"
button3["command"] = update_count

#kick off the event loop
root.mainloop()


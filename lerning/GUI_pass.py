from tkinter import *

class Application(Frame):
    """ A GUI application """

    def __init__(self, master):
        """ Initialize the Frame """
        Frame.__init__(self,master)
        self.grid()
        self.create_widgets()

    def create_widgets(self):
        """ Create button, text and entry widgets """
        self.instruction = Label(self, text = "Enter the pass")
        self.instruction.grid(row = 0, column = 0, columnspan = 2, sticky = W)

        self.password = Entry(self)
        self.password.grid(row = 1, column = 1, sticky = W)

        self.submit_button = Button(self, text="Submit", command = self.reveal)
        self.submit_button.grid(row = 2, column = 0, sticky = W)

        self.text = Text(self, width = 35, height = 5, wrap = WORD)
        self.text.grid(row = 3, column = 0, columnspan = 2, sticky = W)

        
        #check buttons
        Label(self, text = "Chose your movie type").grid(row = 4, column = 0, sticky = W)
        self.comedy = BooleanVar()
        Checkbutton(self, text="Comedy", variable = self.comedy, command = self.update_text).grid(row = 5, column = 0, sticky = W)
        self.drama = BooleanVar()
        Checkbutton(self, text="Drama", variable = self.drama, command = self.update_text).grid(row = 6, column = 0, sticky = W)
        self.romance = BooleanVar()
        Checkbutton(self, text="Romance", variable = self.romance, command = self.update_text).grid(row = 7, column = 0, sticky = W)

        self.result = Text(self, width = 40, height = 5, wrap = WORD)
        self.result.grid(row = 8, column = 0, columnspan = 2, sticky = W)

        #radio buttons
        Label(self,
              text = "Chose your movie type"
              ).grid(row = 9, column = 0, sticky = W)

        self.favorite = StringVar()

        Radiobutton(self,
                    text="Comedy",
                    variable = self.favorite,
                    command = self.update_text2,
                    value="Comedy"
                    ).grid(row = 10, column = 0, sticky = W)
        Radiobutton(self,
                    text="Drama",
                    variable = self.favorite,
                    command = self.update_text2,
                    value = "Drama"
                    ).grid(row = 11, column = 0, sticky = W)
        Radiobutton(self,
                    text="Romance",
                    variable = self.favorite,
                    command = self.update_text2,
                    value = "Romance"
                    ).grid(row = 12, column = 0, sticky = W)
        
    def update_text(self):
        likes = ""

        if self.comedy.get():
            likes += "You like comedy\n"
        
        if self.drama.get():
            likes += "You like drama\n"

        if self.romance.get():
            likes += "You like romantic\n"

        self.result.delete(0.0, END)
        self.result.insert(0.0, likes)

    def update_text2(self):

        self.result.delete(0.0, END)
        self.result.insert(0.0, "Your favorite is " + self.favorite.get())

    def reveal(self):
        """ Displays messagem based on the password tuped in """
        content = self.password.get()

        if content == "password":
            message = "You have access to something special."
        else:
            message = "Access denied."

        self.text.delete(0.0, END)
        self.text.insert(0.0, message)

root = Tk()
root.title("Test GUI")
#root.geometry("250x150")
app = Application(root)

root.mainloop()


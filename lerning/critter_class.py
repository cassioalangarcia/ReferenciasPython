class Critter(object):

    total = 0

    #construtor
    def __init__(self, name, mood):
        print("Construtor")
        self.name = name #public
        Critter.total += 1

        self.__mood = mood #private

    #tostring da classe
    def __str__(self):
        rep = "Objeto Critter, com nome " + self. name
        return rep

    #função pública
    def talk(self):
        #self é a instância
        print("Hi, I'm talking. And my name is", self.name, "and I feel", self.__mood)

    #definindo uma função estática
    def status():
        print("The number is:", Critter.total)
    status = staticmethod(status)
    
    #função privada
    def __privado(self):
        print('Este é um método privado')

    #get
    def get_mood(self):
        return self.__mood

    #set
    def set_mood(self, new_mood):
        if new_mood == "":
            print("Estado não pode ser vazio")
        else:
            self.__mood = new_mood
            print('Estado alterado')

    def get_mood(self):
        return self.__mood

    def set_mood(self, new_mood):
        if new_mood == "":
            print("Estado não pode ser vazio")
        else:
            self.__mood = new_mood
            print('Estado alterado')

    mood = property(get_mood, set_mood)

#main
def main():
    crit = Critter("Dippy", 'good')
    crit.talk()
    print(crit)

    crit2 = Critter("Dippy2", 'fat')
    crit2.talk()
    print(crit2)

    crit3 = Critter("a", 'happy')
    print(crit3._Critter__mood)

    Critter.status()
    print(crit.name)
    crit._Critter__privado()

    print(crit.get_mood())
    crit.set_mood("")
    crit.set_mood("crazy")
    print(crit.get_mood())

    print(crit.mood)
    crit.mood = "testing"
    print(crit.mood)

main()

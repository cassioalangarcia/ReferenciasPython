import os

def rename_files():
    path = r"C:\Users\CássioAlan\Downloads\prank\prank"
    files = os.listdir(path)

    saved_path = os.getcwd()

    os.chdir(path)

    for file in files:
        os.rename(file, ''.join(i for i in file if not i.isdigit()))

    os.chdir(saved_path)

rename_files()

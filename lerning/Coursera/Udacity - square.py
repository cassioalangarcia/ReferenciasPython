import turtle

def draw_square(turtle):
    for i in range(4):
        turtle.forward(100)
        turtle.right(90)

def draw_circle(turtle):
    turtle.circle(100)

def draw_art():
    window = turtle.Screen()
    window.bgcolor('green')

    brad = turtle.Turtle()
    brad.shape("turtle")
    brad.color("blue")
    brad.speed(30)

    for i in range(36):
        draw_square(brad)
        brad.right(10)

    angie = turtle.Turtle()
    angie.shape("arrow")
    angie.color("red")
    angie.speed(30)
    draw_circle(angie)

    window.exitonclick()
    
draw_art()

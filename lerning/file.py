
#manipulacao de arquivos

print('Lendo arquivos por caracteres')
text_file = open("C:/Python34/cassio/arquivoTeste.txt")
print(text_file.read(1))
print(text_file.read(5))
text_file.close()

print('Lendo arquivo completo')
text_file = open('C:/Python34/cassio/arquivoTeste.txt')
print(text_file.read())
text_file.close()

print('Lendo uma linha')
text_file = open('C:/Python34/cassio/arquivoTeste.txt')
print(text_file.readline())
print(text_file.readline())
text_file.close()

print('Escrevendo arquivo')
text_file = open('C:/Python34/cassio/escrevendo.txt', 'w')
text_file.write('Linha 1\n')
text_file.write('Linha 2\n')
text_file.write('Linha 3\n')
text_file.close();
text_file = open('C:/Python34/cassio/escrevendo.txt')
print(text_file.read())
text_file.close();

print('Criando linha')
text_file = open('C:/Python34/cassio/escrevendo.txt', 'w')
lines = ['Linha 1\n',
          'Linha 2\n',
          'Linha 3\n',
          'Linha 4\n']
text_file.writelines(lines)
text_file.close();
text_file = open('C:/Python34/cassio/escrevendo.txt')
print(text_file.read())
text_file.close();
